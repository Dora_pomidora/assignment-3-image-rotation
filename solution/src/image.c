#include "image.h"

struct image *create_image(uint64_t width, uint64_t height) {
    struct image* image = malloc(sizeof(struct image));
    image->width = width;
    image->height = height;
    image->data = malloc(sizeof (struct pixel) * width * height);
    return image;
}

void image_free(struct image *image) {
    free(image->data);
    free(image);
}

struct image *image_rotate(struct image *image) {
    struct image* new_image = create_image(image->height, image->width);
    size_t pixel_counter = 0;
    for (size_t i = 0; i < image->width; ++i)
        for (size_t j = 0; j < image->height; ++j)
            new_image->data[pixel_counter++] = image->data[i + (image->height - j - 1) * image->width];

    return new_image;
}
