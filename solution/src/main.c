#include "image.h"
#include "imageRW.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv;
    if(argc != 3){
        fprintf(stderr, "The number of arguments must be equal to 2. Expected input.bmp output.bmp");
        return -1;
    }

    FILE* r_file = fopen(argv[1], "rb");


    if(r_file == NULL){
        fprintf(stderr, "Input file not found.");
        return 10;
    }
    FILE* w_file = fopen(argv[2], "wb");


    if(w_file == NULL){
        fprintf(stderr, "Output file not found.");
        fclose(r_file);
        return 11;
    }

    struct image* image = NULL;
    enum read_status r_status = from_bmp(r_file, &image);
    if(r_status != READ_OK){
        fprintf(stderr, "bmp read error");
        return r_status;
    }

    struct image* rotated_image = image_rotate(image);

    enum write_status w_status = to_bmp(w_file, rotated_image);
    if(w_status != WRITE_OK){
        fprintf(stderr, "bmp write error");
        return w_status;
    }

    fclose(r_file);
    fclose(w_file);
    return 0;
}
