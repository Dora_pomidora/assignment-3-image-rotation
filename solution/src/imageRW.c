#include "imageRW.h"

static int64_t get_padding(size_t width){
    return 4 - (int64_t)((width * sizeof(struct pixel)) % 4);
}

static struct bmp_header get_header(const struct image* const img){
    return (struct bmp_header){
            .bfType = 0x4d42,
            .bfileSize = img->width * img->height * sizeof(struct pixel) + sizeof(struct bmp_header) + get_padding(img->width) * img->height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = img->height * (img->width + get_padding(img->width)),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
}

enum read_status from_bmp(FILE *in, struct image **img) {
    struct bmp_header header;
    if(!fread(&header, sizeof (struct bmp_header), 1, in))
        return READ_INVALID_HEADER;

    *img = create_image(header.biWidth, header.biHeight);
    int64_t padding = get_padding(header.biWidth);
    size_t next_pixel_number = 0;
    for (size_t i = 0; i < header.biHeight; ++i) {
        for (int j = 0; j < header.biWidth; ++j) {
            struct pixel* pixel_to_read = ((**img).data + next_pixel_number);
            size_t readed = fread(pixel_to_read, sizeof (struct pixel), 1, in);
            if(readed == 0){
                image_free(img);
                return READ_INVALID_BITS;
            }
            next_pixel_number += 1;
        }
        if(fseek(in, padding, SEEK_CUR)){
            image_free(img);
            return READ_INVALID_SIGNATURE;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, const struct image *img) {
    struct bmp_header header = get_header(img);
    if(!fwrite(&header, sizeof (struct bmp_header), 1, out))
        return WRITE_HEADER_ERROR;

    int8_t byte = 0;
    size_t padding = get_padding(img->width);
    size_t next_pixel_number = 0;
    for (size_t i = 0; i < header.biHeight; ++i) {
        struct pixel *pixels_to_write = (img->data + next_pixel_number);
        size_t writed = fwrite(pixels_to_write, sizeof (struct pixel), header.biWidth, out);
        if(writed < header.biWidth){
            return WRITE_ERROR;
        }
        next_pixel_number += header.biWidth;
        for (int j = 0; j < padding; ++j) {
            if(!fwrite(&byte, sizeof (int8_t), 1, out)){
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}
