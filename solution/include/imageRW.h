#ifndef IMAGE_TRANSFORMER_IMAGERW_H
#define IMAGE_TRANSFORMER_IMAGERW_H
#include "image.h"
#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>


enum read_status  {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};
enum  write_status  {
    WRITE_OK,
    WRITE_HEADER_ERROR,
    WRITE_ERROR
};

enum read_status from_bmp( FILE* in, struct image** img );
enum write_status to_bmp( FILE* out, struct image const* img );

#endif //IMAGE_TRANSFORMER_IMAGERW_H
